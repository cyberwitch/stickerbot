FROM python:3.8
ENV PYTHONUNBUFFERED 1
ENV PYTHONWRITEBYTECODE 1

RUN curl -Lo /ngrok.zip https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip \
    && unzip -o /ngrok.zip -d /bin \
    && rm -f /ngrok.zip

ENV APP_HOME=/app

WORKDIR $APP_HOME

COPY . $APP_HOME

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

RUN chmod +x $APP_HOME/entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]
