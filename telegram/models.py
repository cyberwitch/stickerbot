from django.contrib.auth import get_user_model
from django.db import models


class StickerSet(models.Model):
    users = models.ManyToManyField(get_user_model(), related_name="sticker_sets")

    name = models.TextField(unique=True)
    title = models.TextField()
    thumbnail = models.ImageField(upload_to="stickers/")

class Sticker(models.Model):
    sticker_set = models.ForeignKey(StickerSet, on_delete=models.CASCADE, related_name="stickers")

    emoji = models.TextField()
    thumbnail = models.ImageField(upload_to="stickers/")
    sticker = models.ImageField(upload_to="stickers/")

    class Meta:
        unique_together = ("sticker_set", "emoji")
