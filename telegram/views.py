import json

from django.contrib.sites.models import Site
from django.db.transaction import atomic
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from authentication.models import TelegramUser
from telegram.models import StickerSet
from telegram.tasks import ImportStickerSetThread
from telegram.utils import send_message


@csrf_exempt
@atomic
def telegram_webhook(request):
    data = json.loads(request.body)

    try:
        user = TelegramUser.objects.get(telegram_id=data["message"]["from"]["id"]).user
    except TelegramUser.DoesNotExist:
        send_message(
            data["message"]["chat"]["id"],
           f"Who dis? Import your stickers at https://{Site.objects.first().domain}!"
        )
    else:
        if "message" in data and "sticker" in data["message"]:
            try:
                sticker_set = StickerSet.objects.get(name=data["message"]["sticker"]["set_name"])
                sticker_set.users.add(user)
                send_message(data["message"]["chat"]["id"], "Got it - nice! Send me more if you've got 'em!")
            except StickerSet.DoesNotExist:
                sticker_set = StickerSet.objects.create(name=data["message"]["sticker"]["set_name"])
                sticker_set.users.add(user)
                ImportStickerSetThread(sticker_set.pk, data["message"]["chat"]["id"]).run()
        else:
            send_message(
                data["message"]["chat"]["id"],
                f"Hey! Just send me a sticker and I'll import the whole pack for you."
            )

    return HttpResponse(status=200)
