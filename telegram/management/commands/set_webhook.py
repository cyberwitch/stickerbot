import requests
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand
from django.urls import reverse


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--domain")

    def handle(self, *args, **options):
        if options["domain"]:
            domain = options["domain"]
        else:
            domain = Site.objects.first().domain

        url = f"https://{domain}{reverse('telegram_webhook')}"

        print(f"Setting Telegram webhook to {url}")

        response = requests.get(
            f"https://api.telegram.org/bot{settings.TELEGRAM_BOT_TOKEN}/setWebhook",
            params={"url": url},
        )
        print(response.content)
