from django.urls import path

from telegram import views

urlpatterns = [
    path("telegram/webhook/", views.telegram_webhook, name="telegram_webhook"),
]
