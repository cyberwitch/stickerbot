from threading import Thread

from django.core.exceptions import ObjectDoesNotExist

from telegram.models import StickerSet, Sticker
from telegram.utils import get_sticker_set, get_file_url, save_file, send_message


def import_sticker_set(pk, chat_id):
    send_message(chat_id, "Awesome, I'll let you know when I'm done importing. This could take a few minutes...")

    sticker_set = StickerSet.objects.get(pk=pk)
    name = sticker_set.name
    data = get_sticker_set(name)["result"]

    if sticker_set.title != data["title"]:
        sticker_set.title = data["title"]
        sticker_set.save()

    if "thumb" in data:
        save_file(get_file_url(data["thumb"]["file_id"]), sticker_set.thumbnail)

    for sticker_data in data["stickers"]:
        try:
            sticker = sticker_set.stickers.get(emoji=sticker_data["emoji"])
        except ObjectDoesNotExist:
            sticker = Sticker.objects.create(sticker_set=sticker_set, emoji=sticker_data["emoji"])

        if "thumb" in sticker_data:
            save_file(get_file_url(sticker_data["thumb"]["file_id"]), sticker.thumbnail)

        save_file(get_file_url(sticker_data["file_id"]), sticker.sticker)

    send_message(chat_id, f"Got it! I finished importing \"{sticker_set.title}\"")

class ImportStickerSetThread(Thread):
    def __init__(self, pk, chat_id, *args, **kwargs):
        self.pk = pk
        self.chat_id = chat_id
        super().__init__(*args, **kwargs)

    def run(self):
        import_sticker_set(self.pk, self.chat_id)
