import json
import os
import tempfile
import urllib.request

import requests
from django.conf import settings
from django.core.files import File
from lottie.exporters.gif import export_gif
from lottie.parsers.tgs import parse_tgs


def get_sticker_set(name):
    return json.loads(requests.get(
        f"https://api.telegram.org/bot{settings.TELEGRAM_BOT_TOKEN}/getStickerSet",
        params={"name": name},
    ).content)

def get_file_url(file_id):
    file_path = json.loads(requests.get(
        f"https://api.telegram.org/bot{settings.TELEGRAM_BOT_TOKEN}/getFile",
        params={"file_id": file_id},
    ).content)["result"]["file_path"]

    return f"https://api.telegram.org/file/bot{settings.TELEGRAM_BOT_TOKEN}/{file_path}"

def save_file(file_url, file_field):
    file = tempfile.NamedTemporaryFile()
    if file_url[-4:] == "webp":
        urllib.request.urlretrieve(file_url, file.name)
        new_filename = file_url
    else:
        tgs_file = tempfile.NamedTemporaryFile()
        urllib.request.urlretrieve(file_url, tgs_file.name)
        export_gif(parse_tgs(tgs_file.name), file)
        new_filename = f"{file_url}.gif"
    with open(file.name, "rb") as opened_file:
        file_field.save(os.path.basename(new_filename), File(opened_file))

def send_message(chat_id, text):
    requests.get(
        f"https://api.telegram.org/bot{settings.TELEGRAM_BOT_TOKEN}/sendMessage",
        params={
            "chat_id": chat_id,
            "text": text,
        },
    )
