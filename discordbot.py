import discord
import django
from asgiref.sync import sync_to_async
from django.conf import settings
from django.urls import reverse
from hashids import Hashids


hashids = Hashids(salt=settings.SECRET_KEY, min_length=settings.HASH_LENGTH)

class DiscordBot(discord.Client):
    async def on_ready(self):
        for guild in self.guilds:
            for webhook in await guild.webhooks():
                if webhook.user.id == self.user.id:
                    await webhook.delete()

        print("Logged on as {0}!".format(self.user))

    @sync_to_async
    def get_anon_url(self):
        from django.contrib.sites.models import Site

        return f"Who dis? Import your stickers at https://{Site.objects.first().domain}!"

    @sync_to_async
    def get_url(self, user_id, webhook_url):
        from authentication.models import Webhook
        from django.contrib.sites.models import Site
        from social_django.models import UserSocialAuth

        django_user_id = UserSocialAuth.objects.get(uid=user_id).user_id

        Webhook.objects.filter(user_id=django_user_id).delete()
        Webhook.objects.create(user_id=django_user_id, url=webhook_url)

        return f"https://{Site.objects.first().domain}" \
               f"{reverse('sticker_sets', kwargs={'slug': hashids.encode(django_user_id)})}"

    async def on_message(self, message: discord.Message):
        user: discord.User = message.author

        if user.id == self.user.id:
            return

        if message.channel.type == discord.ChannelType.text:
            channel: discord.TextChannel = message.channel

            if self.user in message.mentions:
                avatar = await user.avatar_url.read()
                webhook: discord.Webhook = await channel.create_webhook(name=user.display_name, avatar=avatar)

                try:
                    await user.send(await self.get_url(user.id, webhook.url))
                except:
                    await user.send(await self.get_anon_url())
                else:
                    try:
                        await message.delete()
                    except (discord.Forbidden, discord.NotFound, discord.HTTPException):
                        pass
        elif message.channel.type == discord.ChannelType.private:
            await user.send(await self.get_anon_url())

if __name__ == "__main__":
    django.setup()
    client = DiscordBot()
    client.run(settings.DISCORD_BOT_TOKEN)
