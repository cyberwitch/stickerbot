from django.urls import path

from authentication import views

urlpatterns = [
    path("", views.home),
    path("accounts/logout/", views.discord_logout, name="discord_logout"),
    path("accounts/profile/", views.profile, name="profile"),
    path("telegram/redirect/", views.telegram_redirect, name="telegram_redirect"),
    path("telegram/unlink/", views.telegram_unlink, name="telegram_unlink"),
    path("1/<slug:slug>/", views.sticker_sets, name="sticker_sets"),
    path("1/<slug:slug>/<int:sticker_set>/", views.stickers, name="stickers"),
    path("1/<slug:slug>/<int:sticker_set>/<int:sticker>/", views.sticker, name="sticker"),
]
