from django.contrib.auth import get_user_model
from django.db import models


class TelegramUser(models.Model):
    user = models.OneToOneField(
        get_user_model(), on_delete=models.CASCADE, related_name="telegram_user"
    )
    telegram_id = models.IntegerField()
    telegram_info = models.JSONField()

class Webhook(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name="webhook")
    url = models.URLField()
