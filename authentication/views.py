import hmac
from hashlib import sha256

import requests
from django.conf import settings
from django.contrib.auth import logout, get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from hashids import Hashids

from authentication import models
from telegram.models import Sticker


def home(request):
    return render(request, "home.html")

def profile(request):
    try:
        telegram_user = request.user.telegram_user
    except ObjectDoesNotExist:
        telegram_user = None

    return render(
        request,
        "profile.html",
        context={
            "sticker_sets": request.user.sticker_sets.all(),
            "telegram_user": telegram_user,
        },
    )

def telegram_redirect(request):
    auth_date = request.GET.get("auth_date")
    first_name = request.GET.get("first_name")
    request_hash = request.GET.get("hash")
    request_id = request.GET.get("id")
    last_name = request.GET.get("last_name")
    photo_url = request.GET.get("photo_url")
    username = request.GET.get("username")

    data_check_string = "\n".join(
        field
        for field in [
            f"auth_date={auth_date}" if auth_date else None,
            f"first_name={first_name}" if first_name else None,
            f"id={request_id}" if request_id else None,
            f"last_name={last_name}" if last_name else None,
            f"photo_url={photo_url}" if photo_url else None,
            f"username={username}" if username else None,
        ]
        if field is not None
    )

    secret_key = sha256()
    secret_key.update(settings.TELEGRAM_BOT_TOKEN.encode("utf-8"))
    digest = hmac.new(secret_key.digest(), digestmod="sha256")
    digest.update(bytes(data_check_string, encoding="utf-8"))

    if digest.hexdigest() == request_hash:
        models.TelegramUser.objects.create(
            user=request.user, telegram_id=request_id, telegram_info=request.GET
        )

        return redirect("profile")

def telegram_unlink(request):
    request.user.telegram_user.delete()

    return redirect("profile")

def discord_logout(request):
    logout(request)
    return HttpResponseRedirect('/')

hashids = Hashids(salt=settings.SECRET_KEY, min_length=settings.HASH_LENGTH)

def sticker_sets(request, slug):
    some_sticker_sets = []

    try:
        some_sticker_sets = get_user_model().objects.get(id=hashids.decode(slug)[0]).sticker_sets.all()
    except:
        pass

    return render(
        request,
        "sticker_sets.html",
        context={
            "slug": slug,
            "sticker_sets": some_sticker_sets,
        },
    )

def stickers(request, slug, sticker_set):
    some_stickers = []

    try:
        some_stickers = get_user_model().objects.get(id=hashids.decode(slug)[0]) \
            .sticker_sets.filter(id=sticker_set).first().stickers.all()
    except:
        pass

    return render(
        request,
        "stickers.html",
        context={
            "slug": slug,
            "sticker_set": sticker_set,
            "stickers": some_stickers,
        }
    )

def sticker(request, slug, sticker_set, sticker):
    webhook = get_user_model().objects.get(id=hashids.decode(slug)[0]).webhook.url
    sticker_obj = Sticker.objects.get(id=sticker).sticker
    requests.post(webhook, files={
        sticker_obj.name: sticker_obj.file
    })
    return render(request, "done.html")
